from typing import Dict, List, Iterable
from django.core.exceptions import PermissionDenied
from django.utils.translation import gettext_lazy as _
from django.http import Http404
from rest_framework import exceptions
from rest_framework import status
from rest_framework.views import set_rollback
from rest_framework.response import Response
from social_core.exceptions import SocialAuthBaseException

__all__ = (
    'exception_handler',
    'standards_exception_handler',
)


def exception_handler(exc, context):

    if isinstance(exc, Http404):
        exc = exceptions.NotFound()
    elif isinstance(exc, PermissionDenied):
        exc = exceptions.PermissionDenied()

    if isinstance(exc, (
            exceptions.APIException,
            SocialAuthBaseException
    )):
        headers = {}
        if getattr(exc, 'auth_header', None):
            headers['WWW-Authenticate'] = exc.auth_header
        if getattr(exc, 'wait', None):
            headers['Retry-After'] = '%d' % exc.wait

        if hasattr(exc, 'detail') and hasattr(exc, 'status_code'):
            if isinstance(exc.detail, (list, dict)):
                data = exc.detail
            else:
                data = {'detail': exc.detail}
            status_code = exc.status_code
        else:
            data = {'detail': str(exc)}
            status_code = status.HTTP_400_BAD_REQUEST

        set_rollback()
        return Response(data, status=status_code, headers=headers)
    return


class ExceptionMessageHandler:
    # Code map
    code_invalid = _('Invalid input.')
    code_parse_error = _('Malformed request.')
    code_authentication_failed = _('Incorrect authentication credentials.')
    code_not_authenticated = _('Authentication credentials were not provided.')
    code_permission_denied = _('You do not have permission to perform this action.')
    code_not_found = _('Not found.')
    code_method_not_allowed = _('Method "{method}" not allowed.')
    code_not_acceptable = _('Could not satisfy the request Accept header.')
    code_unsupported_media_type = _('Unsupported media type "{media_type}" in request.')
    code_throttled = _('Expected available in {wait} seconds.')
    code_error = _('A server error occurred.')

    # Status map
    status_400 = ['invalid', 'parse_error']
    status_401 = ['authentication_failed', 'not_authenticated']
    status_403 = ['permission_denied']
    status_404 = ['not_found']
    status_405 = ['method_not_allowed']
    status_406 = ['not_acceptable']
    status_415 = ['unsupported_media_type']
    status_429 = ['throttled']
    status_500 = ['error']

    def get_code_message(self, code: str):
        return getattr(self, f'code_{code}', None)

    def get_code_range(self, status: int):
        return getattr(self, f'status_{status}', None)

    def __call__(self, status: int, code: str = None, context: Dict={}):
        message = ''
        if code:
            message = self.get_code_message(code)

        code_rande = self.get_code_range(status)
        if not message and code_rande:
            message = self.get_code_message(code_rande[0])
        return message.format(**context)


exception_message_handler = ExceptionMessageHandler()


class StandardsExceptionHandler:

    def __call__(self, exc, context):
        self.request = context.get('request')
        self.view = context.get('view')
        self.response = exception_handler(exc, context)
        self.exc = exc
        if self.response:
            if isinstance(self.exc, exceptions.APIException) and hasattr(exc, 'detail'):
                self.details = self.exc.detail
                self.response.data = self.get_response_data()

            elif isinstance(self.exc, SocialAuthBaseException):
                self.response.data = self.get_social_exception_response_data()
        return self.response

    def get_social_exception_response_data(self):
        data = {}
        response_message = 'social_auth_error'
        nested_message = str(self.exc)
        data['code'] = self.response.status_code
        data['message'] = response_message
        data['errors'] = [
            {
                'message': nested_message,
                'domain': 'social_auth_process',
                'reason': self.exc.__class__.__name__
            }
        ]
        return data

    def get_response_data(self):
        data = {
            'code': self.response.status_code,
            'message': self.response.status_text
        }
        errors = self.get_errors(self.details) or []
        if errors:
            data['errors'] = [{
                'message': self.get_exception_message(errors),
                'domain': self.get_domain(errors),
                'reason': self.get_exception_reason(errors),
                'state': errors,
            }]
        return data

    def get_exception_message(self, errors: Iterable):
        return exception_message_handler(
            self.response.status_code,
            getattr(self.exc, 'default_code', None),
            {
                'method': self.request.method,
                'media_type': self.request.content_type,
                'wait': self.get_throttle_duration(),
            }
        )

    def get_throttle_duration(self):
        throttle_durations = []
        for throttle in self.view.get_throttles():
            if not throttle.allow_request(self.request, self.view):
                throttle_durations.append(throttle.wait())

        duration = None
        if throttle_durations:
            durations = [
                duration for duration in throttle_durations
                if duration is not None
            ]
            duration = max(durations, default=None)
        return duration

    def get_domain(self, errors: Iterable) -> str:
        return 'request'

    def get_exception_reason(self, errors: Iterable):
        return getattr(self.exc, 'default_code', self.exc.__class__.__name__)

    def get_errors(self, errors, path=None) -> List:
        if isinstance(errors, dict):
            return self.normalize_dict_errors(errors, path)
        elif isinstance(errors, list):
            return self.normalize_list_errors(errors, path)
        else:
            return self.get_error_block(errors)

    def normalize_dict_errors(self, errors: Dict, path=None) -> Dict:
        return {
            field: self.get_errors(error, field)
            for field, error in errors.items()
        }

    def normalize_list_errors(self, errors: List, path=None) -> List:
        return [
            self.get_errors(error)
            for error in errors
        ]

    def get_error_block(self, error) -> Dict:
        return {'message': str(error), 'reason': error.code}


standards_exception_handler = StandardsExceptionHandler()

