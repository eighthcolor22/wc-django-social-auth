from django.db import transaction
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.views.decorators.cache import never_cache
from django.utils.decorators import method_decorator
from django.conf import settings

from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import GenericAPIView
from rest_framework.serializers import Serializer
from rest_framework.response import Response
from social_core.utils import setting_name
from social_django.utils import psa

from wcd_social_auth.actions import (
    do_auth,
    do_complete,
    do_disconnect,
    do_login
)

from wcd_social_auth.responses import AuthActionResponse
from wcd_social_auth.rest.exception_handlers import (
    exception_handler,
    standards_exception_handler,
)
from wcd_social_auth.utils import get_error_redirect_url, load_api_strategy, rest_psa


API_NAMESPACE = (
        getattr(settings, setting_name('API_URL_NAMESPACE'), None) or
        'wcd_social_auth_api'
)

USE_STANDARDS = getattr(settings, setting_name('USE_STANDARDS'), True)

USE_JWT_TOKENS_RESPONSE = (
  getattr(settings, setting_name('USE_JWT_TOKENS_RESPONSE'), True) and
  ('wcd_jwt' in settings.INSTALLED_APPS or 'rest_framework_simplejwt' in settings.INSTALLED_APPS)
)


API_COMPLETE_REDIRECT_URL = (
    getattr(settings, setting_name('API_COMPLETE_REDIRECT_URL'), None) or
    f'{API_NAMESPACE}:complete'
)


class ResponseMixin(GenericAPIView):
    action_response = None
    queryset = []
    serializer_class = Serializer

    def get_schema_response_data(self, response, data, **kwargs):
        return {
            "code": response.status_code,
            "data": response.data,
        }

    def get_exception_handler(self):
        if USE_STANDARDS:
            return standards_exception_handler
        return exception_handler

    def handle_exception(self, exc):
        response: Response = super().handle_exception(exc)
        response.data['redirect_url'] = get_error_redirect_url(self.request)
        response.data.update(
            self.request.backend.strategy.response_data()
        )
        return response

    def finalize_response(self, request, response: Response, *args, **kwargs):
        if USE_STANDARDS and response.status_code < 400:
            response.data = (
                self.get_schema_response_data(response, response.data, request=request, **kwargs)
            )
        return super().finalize_response(request, response, *args, **kwargs)

    def handle_action_response(self, action_response: AuthActionResponse):
        response_data = action_response.backend.strategy.response_data()
        response_data['redirect_url'] = action_response.redirect_url
        return Response(response_data)


@method_decorator(never_cache, 'dispatch')
@method_decorator(
    rest_psa(API_COMPLETE_REDIRECT_URL, load_strategy=load_api_strategy),
    'dispatch'
)
class AuthBeginAPIView(ResponseMixin):

    def get(self, request, *args, **kwargs):
        backend = do_auth(request.backend, redirect_name=REDIRECT_FIELD_NAME)
        data = {
            'meta': backend.strategy.meta_data(),
            'initial': backend.strategy.initial_data()
        }

        if backend.uses_redirect():
            data['redirect_url'] = backend.auth_url()
        return Response(data)


@method_decorator(never_cache, 'dispatch')
@method_decorator(csrf_exempt, 'dispatch')
@method_decorator(transaction.atomic, 'dispatch')
@method_decorator(
    rest_psa(API_COMPLETE_REDIRECT_URL, load_strategy=load_api_strategy),
    'dispatch'
)
class AuthCompleteAPIView(ResponseMixin):
    serializer_class = Serializer

    @staticmethod
    def get_jwt_tokens(action_response):
        from rest_framework_simplejwt.tokens import RefreshToken

        user = action_response.backend.strategy.request.user
        refresh_token = RefreshToken.for_user(user)
        access_token = refresh_token.access_token
        return {'refresh': str(refresh_token), 'access': str(access_token)}

    def handle_action_response(self, action_response: AuthActionResponse):
        response_data = action_response.backend.strategy.response_data()
        response_data['redirect_url'] = action_response.redirect_url

        if USE_JWT_TOKENS_RESPONSE:
            response_data['jwt'] = self.get_jwt_tokens(action_response)
        return Response(response_data)

    def complete(self, *args, **kwargs):
        """Authentication complete view"""
        complete_response = do_complete(
                self.request.backend,
                do_login,
                user=self.request.user,
                redirect_name=REDIRECT_FIELD_NAME,
                request=self.request,
                **kwargs
            )

        if isinstance(complete_response, AuthActionResponse):
            return self.handle_action_response(complete_response)
        return complete_response

    def get(self, *args, **kwargs):
        return self.complete(*args, **kwargs)

    def post(self, *args, **kwargs):
        return self.complete(*args, **kwargs)


@method_decorator(never_cache, 'dispatch')
@method_decorator(csrf_protect, 'dispatch')
@method_decorator(
    psa(load_strategy=load_api_strategy), 'dispatch'
)
class DisconnectAPIView(
    ResponseMixin,
    GenericAPIView
):
    permission_classes = (IsAuthenticated, )

    def do_disconnect(self, *args, **kwargs):
        association_id = self.kwargs.get('association_id')
        disconnect_response = do_disconnect(
            self.request.backend,
            self.request.user,
            association_id,
            redirect_name=REDIRECT_FIELD_NAME
        )
        if isinstance(disconnect_response, AuthActionResponse):
            return self.handle_action_response(disconnect_response)
        return disconnect_response

    def post(self, *args, **kwargs):
        return self.do_disconnect(*args, *kwargs)

