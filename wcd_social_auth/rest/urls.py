from django.urls import path

from social_django.urls import extra
from . import views

app_name = 'wcd_social_auth_api'

urlpatterns = [
    # authentication / association
    path(f'login/<str:backend>{extra}', views.AuthBeginAPIView.as_view(), name='begin'),
    path(f'complete/<str:backend>{extra}', views.AuthCompleteAPIView.as_view(), name='complete'),
    # disconnection
    path(f'disconnect/<str:backend>{extra}', views.DisconnectAPIView.as_view(), name='disconnect'),
    path(
        f'disconnect/<str:backend>/<int:association_id>{extra}',
        views.DisconnectAPIView.as_view(),
        name='disconnect_individual'
    ),
]