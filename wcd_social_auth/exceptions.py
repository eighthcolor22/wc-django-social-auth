from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError, PermissionDenied
from social_core.exceptions import SocialAuthBaseException
from rest_framework import status

__all__ = (
    'SocialAuthRestValidationException',
    'InactiveUserSocialPermissionDenied',
)


class SocialAuthRestValidationException(
    ValidationError,
    SocialAuthBaseException
):
    pass


class InactiveUserSocialPermissionDenied(PermissionDenied):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = _('User is not active')
    default_code = 'inactive_user'


