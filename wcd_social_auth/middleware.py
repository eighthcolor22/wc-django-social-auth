from social_core.utils import setting_url

from social_django.middleware import SocialAuthExceptionMiddleware
from .exceptions import InactiveUserSocialPermissionDenied
from .utils import get_error_redirect_url


class WCDSocialAuthExceptionMiddleware(SocialAuthExceptionMiddleware):

    def get_redirect_uri(self, request, exception):
        if isinstance(exception, InactiveUserSocialPermissionDenied):
            backend = getattr(request, 'backend', None)
            return setting_url(backend, 'INACTIVE_USER_URL', 'LOGIN_ERROR_URL',
                               'LOGIN_URL')
        return get_error_redirect_url(request)
