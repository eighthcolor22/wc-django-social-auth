from django.urls import path
from social_django.urls import extra

from . import views

app_name = 'wcd_social_auth'

urlpatterns = [
    # authentication / association
    path(f'login/<str:backend>{extra}', views.auth, name='begin'),
    path(f'complete/<str:backend>{extra}', views.complete, name='complete'),
    # disconnection
    path(f'disconnect/<str:backend>{extra}', views.disconnect, name='disconnect'),
    path(f'disconnect/<str:backend>/<int:association_id>{extra}', views.disconnect, name='disconnect_individual'),

    path(f'interrupt/', views.SocialAuthInterruptView.as_view(), name='interrupt'),
]