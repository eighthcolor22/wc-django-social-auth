from django.conf import settings
from django.urls import reverse
from social_core.pipeline.partial import partial
from social_django.views import NAMESPACE
from social_core.utils import setting_name

from .consts import USER_FIELDS

ALLOW_OVERWRITE_USER_FIELDS = (
    getattr(settings, setting_name('ALLOW_OVERWRITE_USER_FIELDS'), False)
)

SHOULD_CREATE_USER = (
    getattr(settings, setting_name('AUTH_SHOULD_CREATE_USER'), True)
)


def get_required_data_error_context(strategy, details, **kwargs):
    backend = kwargs['backend']
    return {
        'submit_url': (
                reverse(f'{NAMESPACE}:complete', args=(backend.name,))
        ),
        'partial_token': kwargs.get('current_partial').token,
        'details': details,
    }


@partial
def required_data_getter(
    strategy,
    details,
    user=None,
    is_new=False,
    serializer_getter_func=None,
    *args,
    **kwargs
):
    required_data_serializer_class = serializer_getter_func(
        strategy, details, user, is_new, *args, **kwargs
    )

    if user and not is_new:
        return

    elif is_new:
        data = {**details, **strategy.request_data()}
        if settings.DEBUG and 'partial_token' not in data:
            remove_fields = getattr(
                settings, setting_name('REMOVE_FIELDS_FOR_DEBUG'), []
            )
            data = {key: val for key, val in data.items() if key not in remove_fields}

        serializer = required_data_serializer_class(
            data=data,
            context={
                'request': strategy.request,
                'strategy': strategy
            }
        )
        if not serializer.is_valid():

            interrupt_data = get_required_data_error_context(
                strategy, details, **kwargs
            )
            interrupt_data.update({'errors': serializer.errors})
            return strategy.interrupt(data=interrupt_data)

        serializer.save(strategy, *args, **kwargs)
        details.update(
            serializer.validated_data
        )


def extend_details_with_initial(strategy, details, backend, user=None, *args, **kwargs):
    details.update(strategy.initial_data())


def set_strategy_next_url(strategy, details, backend, user=None, url_resolver_func=None, *args, **kwargs):
    next_url = strategy.session_get('next')
    if next_url or not url_resolver_func:
        return

    url = url_resolver_func(strategy, details, backend, user, *args, **kwargs)
    strategy.session_set('next', url)


def user_details(strategy, details, backend, user=None, *args, **kwargs):
    """Update user details using data from provider."""
    if not user:
        return

    changed = False  # flag to track changes
    if strategy.setting('NO_DEFAULT_PROTECTED_USER_FIELDS') is True:
        protected = ()
    else:
        protected = ('username', 'id', 'pk', 'email', 'password',
                     'is_active', 'is_staff', 'is_superuser',)

    protected = protected + tuple(strategy.setting('PROTECTED_USER_FIELDS', []))
    field_mapping = strategy.setting('USER_FIELD_MAPPING', {}, backend)
    for name, value in details.items():
        # Convert to existing user field if mapping exists
        name = field_mapping.get(name, name)
        if value is None or not hasattr(user, name) or name in protected:
            continue

        current_value = getattr(user, name, None)
        if current_value == value:
            continue

        if ALLOW_OVERWRITE_USER_FIELDS or not current_value:
            changed = True
            setattr(user, name, value)
            continue

    if changed:
        strategy.storage.user.changed(user)


def receive_avatar_from_social(backend, strategy, details, response, user=None, *args, **kwargs):
    if hasattr(backend, 'receive_avatar_from_social') and user:
        backend.receive_avatar_from_social(user, details, response)


def create_user(strategy, details, backend, user=None, *args, **kwargs):
    if user:
        return {'is_new': False}

    user_fields = backend.setting('USER_FIELDS', USER_FIELDS)
    extra_fields = getattr(settings, setting_name('USER_EXTRA_FIELDS'), [])
    user_fields = [*user_fields, *extra_fields]

    fields = dict(
        (name, kwargs.get(name, details.get(name))) for name in user_fields
    )
    if not fields:
        return
    return_data = {'is_new': True} 
    if SHOULD_CREATE_USER:
        return_data.update(
            {'user': strategy.create_user(**fields)}
        )
    else:
        details.update(return_data)
    return return_data


def set_user_details_to_session(backend, strategy, details, response, user=None, *args, **kwargs):
    strategy.session_set('user_details', details)

