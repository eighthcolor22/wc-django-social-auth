from django.db import transaction
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import login_required
from django.conf import settings as django_settings
from django.core import signing
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.views.decorators.http import require_POST
from django.views.decorators.cache import never_cache
from django.contrib.auth.mixins import AccessMixin

from social_django.utils import psa, load_strategy, Storage
from social_django.views import NAMESPACE
from social_core.utils import setting_name

from .actions import do_auth, do_complete, do_disconnect, do_login
from .strategy import WCDjangoViewsStrategy
from .responses import AuthActionResponse


@never_cache
@psa(f'{NAMESPACE}:complete', load_strategy=load_strategy)
def auth(request, backend):
    backend = do_auth(request.backend, redirect_name=REDIRECT_FIELD_NAME)
    return backend.start()


@never_cache
@csrf_exempt
@transaction.atomic
@psa(f'{NAMESPACE}:complete', load_strategy=load_strategy)
def complete(request, backend, *args, **kwargs):
    """Authentication complete view"""

    complete_response = do_complete(
        request.backend,
        do_login,
        user=request.user,
        redirect_name=REDIRECT_FIELD_NAME,
        request=request,
        *args, **kwargs
    )
    if isinstance(complete_response, AuthActionResponse):
        backend, url = complete_response.backend, complete_response.redirect_url
        return backend.strategy.redirect(url)
    return complete_response


@never_cache
@login_required
@psa()
@require_POST
@csrf_protect
def disconnect(request, backend, association_id=None):
    """Disconnects given backend from current logged in user."""
    disconnect_response = do_disconnect(
        request.backend,
        request.user,
        association_id,
        redirect_name=REDIRECT_FIELD_NAME
    )
    if isinstance(disconnect_response, AuthActionResponse):
        backend, url = disconnect_response.backend, disconnect_response.redirect_url
        return backend.strategy.redirect(url)
    return disconnect_response


class SocialAuthInterruptView(AccessMixin, TemplateView):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.strategy = None
        self.interrupt_data = {}

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_template_names(self):
        return getattr(
            django_settings,
            setting_name('INTERRUPT_TEMPLATE'),
            'wcd_social_auth/social_interrupt_template.jinja'
        )

    def set_interrupt_data(self):
        self.strategy = WCDjangoViewsStrategy(
            storage=Storage,
            request=self.request
        )
        interrupt_data_token = self.strategy.session.get('interrupt_data')
        self.interrupt_data = signing.loads(interrupt_data_token)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.set_interrupt_data()
        partial_token = self.interrupt_data.get('partial_token')
        partial = self.strategy.partial_load(partial_token)
        context["partial_backend_name"] = partial.backend
        context['interrupt_data'] = self.interrupt_data
        return context

