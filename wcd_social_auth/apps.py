from django.apps import AppConfig


class WcdSocialAuthConfig(AppConfig):
    name = 'wcd_social_auth'
