from rest_framework import serializers

__all__ = (
    'SocialAuthDetailBaseSerializer',
)


class SocialAuthDetailBaseSerializer(serializers.Serializer):
    partial_token = serializers.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        data = kwargs.get('data', {})
        if 'partial_token' in data:
            for field in self.fields.values():
                field.required = True

    def validate(self, attrs):
        attrs = super().validate(attrs)
        return attrs

    def save(self, *args, **kwargs):
       pass

    @property
    def errors(self):
        errors = super().errors
        return {
            key: [{'message': str(val[0]), 'reason': val[0].code}]
            for key, val in errors.items()
        }
