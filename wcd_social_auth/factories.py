from typing import Callable
from functools import partial

from .pipeline import (
    required_data_getter,
    set_strategy_next_url,
)

__all__ = (
    'data_validation_factory',
    'next_url_factory',
)


def data_validation_factory(serializer_getter_func: Callable):
    return partial(
        required_data_getter,
        serializer_getter_func=serializer_getter_func
    )


def next_url_factory(url_resolver_func: Callable):
    return partial(
        set_strategy_next_url,
        url_resolver_func=url_resolver_func
    )
