import json

from django.http import JsonResponse
from django.core import signing
from django.urls import reverse
from django.conf import settings as django_settings

from social_django.strategy import DjangoStrategy
from social_core.utils import setting_name


SESSION_STORED_FIELDS_PREFIX = getattr(django_settings, setting_name('SESSION_STORED_FIELDS_PREFIX'),
                                       'wcd_social')


class WCDjangoStrategy(DjangoStrategy):

    def request_data(self, merge=True):
        data = super().request_data(merge)
        request = self.request
        if (
            request and
            request.is_ajax() and
            request.method == 'POST'
        ):
            return json.loads(request.body)
        return data

    def json(self, data, status_code=200):
        return JsonResponse(data=data, status=status_code)

    def get_extra_from_session(self, field_name: str):
        field_name = f'{SESSION_STORED_FIELDS_PREFIX}_{field_name}'
        data = self.session_get(field_name) or b'{}'
        return json.loads(data)

    def meta_data(self):
        return self.get_extra_from_session('meta')

    def initial_data(self):
        return self.get_extra_from_session('initial')

    def response_data(self):
        return {
            'meta': self.meta_data(),
            'initial': self.initial_data(),
            'details': self.session_get('user_details', {}),
        }


class WCDjangoViewsStrategy(WCDjangoStrategy):

    def interrupt(self, data):
        self.session['interrupt_data'] = signing.dumps(data)
        url = getattr(
            django_settings,
            setting_name('INTERRUPT_URL'),
            reverse('wcd_social_auth:interrupt')
        )
        return self.redirect(url)


class WCDjangoAPIStrategy(WCDjangoStrategy):

    def interrupt(self, data):
        status_code = 400
        data['message'] = 'BadRequest'
        data['code'] = status_code
        return self.json(data=data, status_code=status_code)