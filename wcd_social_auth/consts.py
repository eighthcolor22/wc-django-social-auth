__all__ = (
    'SESSION_STORED_FIELDS',
    'USER_FIELDS',
)


SESSION_STORED_FIELDS = ['meta', 'initial']
USER_FIELDS = ['username', 'email']
