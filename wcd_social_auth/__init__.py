__version__ = '0.1'

VERSION = tuple(__version__.split('.'))

default_app_config = 'wcd_social_auth.apps.WcdSocialAuthConfig'