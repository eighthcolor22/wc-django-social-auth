from dataclasses import dataclass

from social_core.backends.base import BaseAuth

__all__ = (
    'AuthActionResponse',
)


@dataclass(frozen=True)
class AuthActionResponse:
    is_success: bool
    backend: BaseAuth
    redirect_url: str
