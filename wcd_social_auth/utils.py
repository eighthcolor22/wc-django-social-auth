from typing import List
from functools import wraps

from django.http import Http404
from django.urls import reverse
from django.http import HttpRequest
from django.conf import settings
from social_core.exceptions import MissingBackend
from social_django.utils import load_backend, load_strategy as default_load_strategy
from social_core.utils import (
    setting_name,
    get_strategy,
    module_member
)

__all__ = (
    'load_api_strategy',
    'get_error_redirect_url',
    'run_login_pipeline',
    'rest_psa',
)

API_STRATEGY = getattr(settings, setting_name('API_STRATEGY'),
                       'wcd_social_auth.strategy.WCDjangoAPIStrategy')
STORAGE = getattr(settings, setting_name('STORAGE'),
                  'social_django.models.DjangoStorage')


def load_api_strategy(request=None):
    return get_strategy(API_STRATEGY, STORAGE, request)


def get_error_redirect_url(request: HttpRequest):
    strategy = getattr(request, 'social_strategy', None)
    return strategy.setting('LOGIN_ERROR_URL')


def _run_pipeline(pipeline: List, **kwargs):
    for name in pipeline:
        func = module_member(name)
        func(**kwargs)


def run_login_pipeline(
        request_after: 'HttpRequest',
        session_key_before: str
):
    login_pypline = getattr(settings, setting_name('LOGIN_PIPLINE'), [])
    return _run_pipeline(
        login_pypline,
        request_after=request_after,
        session_key_before=session_key_before
    )


def rest_psa(redirect_uri=None, load_strategy=default_load_strategy):
    def decorator(func):
        @wraps(func)
        def wrapper(request, backend, *args, **kwargs):
            uri = redirect_uri

            if uri and uri.startswith('/') and '{backend}' in uri:
                uri = uri.format(backend=backend)

            elif uri and not uri.startswith('/'):
                uri = reverse(redirect_uri, args=(backend,))
            request.social_strategy = load_strategy(request)
            # backward compatibility in attribute name, only if not already
            # defined
            if not hasattr(request, 'strategy'):
                request.strategy = request.social_strategy

            try:
                request.backend = load_backend(request.social_strategy,
                                               backend,
                                               redirect_uri=uri)
            except MissingBackend:
                raise Http404('Backend not found')
            return func(request, backend, *args, **kwargs)
        return wrapper
    return decorator
