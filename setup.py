import os
import re
import setuptools


with open('README.md', 'r') as fh:
    long_description = fh.read()


def get_version(package):
    """
    Return package version as listed in `__version__` in `init.py`.
    """
    init_py = open(os.path.join(package, '__init__.py')).read()

    return re.search('__version__ = [\'"]([^\'"]+)[\'"]', init_py).group(1)


# version = get_version('wcd_user_checks')


setuptools.setup(
    name='wc-django-social-auth',
    version=get_version('wcd_social_auth'),
    author='WebCase',
    author_email='info@webcase.studio',
    license='MIT License',
    description='Package to provide additional social auth cases.',
    install_requires=(
        'social-auth-app-django>=5.0.0,<6.0',
        'djangorestframework>=3.0,<4.0',
        'djangorestframework-simplejwt>=5.1.0,<6.0.0',
    ),
    extras_require={},
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=setuptools.find_packages(exclude=(
        'tests', 'tests.*',
    )),
    python_requires='>=3.7',
    classifiers=(
        'Development Status :: 2 - Pre-Alpha',

        'Programming Language :: Python :: 3',

        'Intended Audience :: Developers',
        'Topic :: Utilities',

        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ),
    include_package_data=True
)
