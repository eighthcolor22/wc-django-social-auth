# WebCase Social Auth

Package to provide additional social auth cases based on Python Social Auth https://python-social-auth.readthedocs.io/en/latest/index.html.

See extended docs at: https://www.dropbox.com/scl/fi/f47lhmzl2e0cxhd0e3dvc/%D0%A1%D0%BF%D0%B5%D1%86%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%86%D0%B8%D1%8F-Social-Auth.paper?dl=0&rlkey=v5rwwvfl8lpdxtg26e70pr6af

## Installation
pip
```shell
pip install git+https://gitlab.com/eighthcolor22/wc-django-social-auth.git
```
pipenv
```shell
pipenv install git+https://gitlab.com/eighthcolor22/wc-django-social-auth.git#egg=wc-django-social-auth
```
poetry
```shell
poetry add git+https://gitlab.com/eighthcolor22/wc-django-social-auth.git#master
```
pip


## Confs:
### Add next to apps in settings:
```python
PROJECT_APPS = [
    ...
    'social_django',
    'wcd_social_auth',
    ...
]

SOCIAL_AUTH_URL_NAMESPACE = 'wcd_social_auth'
SOCIAL_AUTH_STRATEGY = 'wcd_social_auth.strategy.WCDjangoViewsStrategy'

MIDDLEWARE += [
    'wcd_social_auth.middleware.WCDSocialAuthExceptionMiddleware',
]

 ```
```python
### urls.py
    path('', include('wcd_social_auth.urls')),
    path('api/v1/', include('wcd_social_auth.rest.urls')), # if use REST
```
### Read about Python Social Auth configs on https://python-social-auth.readthedocs.io/en/latest/configuration/index.html

### Available settings 

`SOCIAL_AUTH_USER_IS_ACTIVE_VALIDATOR` - path to custom user active validator func. Args: `user` (type: AuthUserModel). Default - `social_django.utils.user_is_active`. To customize is active error response, raise `wcd_social_auth.exceptions.InactiveUserSocialAuthException` class exception.   

`SOCIAL_AUTH_INTERRUPT_TEMPLATE` - path to custom template for auth interrupt view. Default template `wcd_social_auth.templates.wcd_social_auth.social_interrupt_template.jinja` has simple form with email field.

`SOCIAL_AUTH_INTERRUPT_URL` - url to redirect when social auth interrupts to enter some required user data. Default `reverse('wcd-social-auth:interrupt')`.

`SOCIAL_AUTH_ALLOW_OVERWRITE_USER_FIELDS` - sets overwriting user details using data from provider. If `True`,
every social authorization will overwrite user details. If `False`, only not filled user details will be overwritten. Default `False`.

`SOCIAL_AUTH_SHOULD_CREATE_USER` - turns off creating user in `wcd_social_auth.pipeline.create_user` pipeline function. It can be used for creating user manually. Default `True`.

`SOCIAL_AUTH_API_URL_NAMESPACE` - used to reverse 'complete url' if api views were rewritten. Default `wcd_social_auth_api`.

`SOCIAL_AUTH_API_COMPLETE_REDIRECT_URL` - is used to rewrite complete api redirect url. Default `None`.

`SOCIAL_AUTH_API_STRATEGY` - is used to rewrite default api strategy. Default `wcd_social_auth.strategy.WCDjangoAPIStrategy`.

`SOCIAL_AUTH_SESSION_STORED_FIELDS_PREFIX` - prefix for initial and meta data names in sessions. Default `wcd_social`.
`SOCIAL_AUTH_USER_EXTRA_FIELDS` - list of additional user model fields that can be filled in while user creation . Default `[]`.
`SOCIAL_AUTH_LOGIN_PIPLINE` - list of functions will be called after login. Used to perform special actions that require a session key before authorization and after. Arguments are the `request_after` - django Http request after login and `session_key_before` - session key before login. Default `[]`.
`SOCIAL_AUTH_USE_JWT_TOKENS_RESPONSE` - if it is `True` and `wc-django-jwt` or `djangorestframework-simplejwt` is installed REST success authentication response will include JWT tokens. Default `False`.

### Available pipeline functions

`wcd_social_auth.pipeline.extend_details_with_initial` - pipeline function that update social details dict with initial user data.

`wcd_social_auth.pipeline.user_details` - provides overwriting user details opportunity.

`wcd_social_auth.pipeline.create_user` - changed default `social_core.pipeline.user.create_user` func to provide manual user creating.

`wcd_social_auth.pipeline.receive_avatar_from_social` - provides receiving user avatar image from socials. To use it you have to override 
backend class with new method `receive_avatar_from_social`, that will get image from social response. Args: user  (type: AuthUserModel), details (type: dict), response (type: dict)

### Added fabric-functions, to customize pipeline:

`wcd_social_auth.factories.data_validation_factory`- provides creating social data validation pipeline func with your own logic of validation. 
Args: `serializer_getter_func`(type: Callable). 
Function receive `strategy, details, user, is_new, *args, **kwargs` as args, kwargs. 
Must return subclass of `wcd_social_auth.serializers import SocialAuthDetailBaseSerializer`.


```python
from wcd_social_auth.factories import data_validation_factory
from wcd_social_auth.serializers import SocialAuthDetailBaseSerializer

class SocialAuthDataValidationSerializer(SocialAuthDetailBaseSerializer):
    pass


def serializer_getter_func(*args, **kwargs):
    return SocialAuthDataValidationSerializer


required_data_getter = data_validation_factory(
    serializer_getter_func=serializer_getter_func
)
```

`wcd_social_auth.factories.next_url_factory` - provides creating pipeline func with own redirect logic. Args: `get_next_url`(type: Callable). 
Function receive `strategy, backend, user, *args, **kwargs` as args, kwargs. Must return url

```python
from wcd_social_auth.factories import next_url_factory


def get_next_url(*args, **kwargs):
    return reverse('index')


next_url_getter = next_url_factory(get_next_url)
```

`wcd_social_auth.views.SocialAuthInterruptView` - is used to interrupt authentication, render forms to receive extra user details.
Context vars:  `partial_backend_name` - social backend name, `partial_token` - token, required to be sent to continue auth process,
`interrupt_data` - dict with user details, errors, submit url, etc.

## Base usage example
### Unique user identifier is email. Use GoogleOAuth2 and FacebookOAuth2

#### settings
```python
MIDDLEWARE += [
    'social_django.middleware.SocialAuthExceptionMiddleware',
]

TEMPLATES[0]['OPTIONS']['context_processors'] += [

    'social_django.context_processors.backends',
    'social_django.context_processors.login_redirect',
]

AUTHENTICATION_BACKENDS = (
    'social_core.backends.facebook.FacebookOAuth2',
    'social_core.backends.google.GoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)

USERNAME_IS_FULL_EMAIL = True


SOCIAL_AUTH_FACEBOOK_KEY = env('SOCIAL_AUTH_FACEBOOK_KEY')
SOCIAL_AUTH_FACEBOOK_SECRET = env('SOCIAL_AUTH_FACEBOOK_SECRET')
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = env('SOCIAL_AUTH_GOOGLE_OAUTH2_KEY')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = env('SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET')


SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'openid',
    'email',
    'profile',
]


SOCIAL_AUTH_FACEBOOK_API_VERSION = '2.10'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email', 'public_profile']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'locale': 'ru_RU',
    'fields': 'id, name, email, picture.type(large), link'
}
SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [
    ('name', 'name'),
    ('email', 'email'),
    ('picture', 'picture'),
    ('link', 'profile_url'),
]

# 
SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    'social_core.pipeline.social_auth.social_user',

    'apps.your_app.social_pipeline.required_data_getter',
    
    'wcd_social_auth.pipeline.extend_details_with_initial',
    'social_core.pipeline.social_auth.associate_by_email',
    'social_core.pipeline.user.get_username',
    'wcd_social_auth.pipeline.create_user',

    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'wcd_social_auth.pipeline.user_details',
    # 'wcd_social_auth.pipeline.receive_avatar_from_social',
    
    'apps.your_app.social_pipeline.next_url_getter',
    
    'wcd_social_auth.pipeline.set_user_details_to_session', # if need 
)

SOCIAL_AUTH_URL_NAMESPACE = 'wcd_social_auth'

```

#### someapp.social_pipeline.py

```python
from rest_framework import serializers
from wcd_social_auth.factories import data_validation_factory, next_url_factory
from wcd_social_auth.serializers import SocialAuthDetailBaseSerializer



class SocialAuthDataValidationSerializer(SocialAuthDetailBaseSerializer):
    email = serializers.EmailField(required=True)
    #email_verification_code = serializer.CharField(required=False)
    
    def save(self, **kwargs):
        """
        Method save calls when serializer is valid. You can add some actions, 
        e.g. submit email code(get from self.validated_data) 
        """        
        pass
        
    


def serializer_getter_func(*args, **kwargs):
    return SocialAuthDataValidationSerializer


required_data_getter = data_validation_factory(serializer_getter_func)


def get_next_url(*args, **kwargs):
    return '/next-url/'


next_url_getter= next_url_factory(get_next_url)
```
